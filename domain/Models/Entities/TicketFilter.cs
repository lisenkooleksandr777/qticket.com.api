﻿using System;
namespace domain.Models.Entities
{
    public class TicketFilter
    {
        public int UserId { get; set; }

        public int Page { get; set; }

        public int ItemsPerPage { get {
                return 10;
            }
        }

        public DateTime? From { get; set; }

        public DateTime? To { get; set; }
    }
}
