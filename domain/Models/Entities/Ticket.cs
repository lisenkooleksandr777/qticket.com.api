﻿using System;
namespace domain.Models.Entities
{
    public class Ticket
    {
        public int Id { get; set; }

        public EventTicket Event {get;set;}

        public User Owner { get; set; }

        public string VisitorName {get;set;}
    }   
}
