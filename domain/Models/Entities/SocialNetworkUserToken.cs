﻿using System;

namespace domain.Models.Entities
{
    public class SocialNetworkUserToken
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Provider { get; set; }
        public string TokenString { get; set; }
        public string TokenId { get; set; }
        public DateTime DataAccessExpirationDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime RefreshDate { get; set; }
    }
}
