﻿using System;
namespace domain.Models.Entities
{
    public class EventTicketAnalytic
    {
        public EventTicket EventTicket { get; set; }

        public int AmountOfAvailableTickets { get; set; }
    }
}
