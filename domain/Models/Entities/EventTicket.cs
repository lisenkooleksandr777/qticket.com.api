﻿using System;
namespace domain.Models.Entities
{
    
    public class EventTicket
    {
        public int Id { get; set; }
        //refer to user table
        public User Owner {get; set;}

        public string Name { get; set; }

        public string Description { get; set; }

        public int Amount { get; set; }

        public decimal Price { get; set; }

        public DateTime DateStart { get; set; }

        public DateTime DateEnd { get; set; }
    }
}
