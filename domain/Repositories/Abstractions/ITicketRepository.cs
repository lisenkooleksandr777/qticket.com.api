﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using domain.Models.Entities;

namespace domain.Repositories.Abstractions
{
    public interface ITicketRepository
    {
        Ticket BookTicket(Ticket ticket);

        List<Ticket> GetTickets(TicketFilter filter);

        Ticket GetTicket(int ownerId, int ticketId);

        bool IsRealOwner(int ownerId, int ticketId);
    }
}
