﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using domain.Models.Entities;

namespace domain.Repositories.Abstractions
{
    
    public interface ITicketAgregator
    {
        EventTicket Create(EventTicket ticket);

        Task<EventTicket> Update(EventTicket ticket);

        Task<EventTicket> Delete(int ticketId, int ownerId);

        Task<EventTicket> Get(int ticketId);

        Ticket ChekUserTicket(int ownerId, int ticketId);

        EventTicketAnalytic GetAnalytic(int ticketId);

        List<EventTicket> Get(int ownerId, int page, int itemsPerPage);
    }
}
