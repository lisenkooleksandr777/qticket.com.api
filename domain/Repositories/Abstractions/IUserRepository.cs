﻿using System;
using System.Threading.Tasks;
using domain.Models.Entities;

namespace domain.Repositories.Abstractions
{
    public interface IUserRepository
    {
        Task<User> Create(User user);

        Task<User> Update(User user);

        Task<User> Get(int userId);

        Task<User> Get(string socialId, string provider);
    }
}
