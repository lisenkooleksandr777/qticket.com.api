﻿using System;
using System.Collections.Generic;
using service.Abstractions;
using service.Services;

namespace service
{
    public static class Module
    {
        public static Dictionary<Type, Type> GetDependencies()
        {
            var result = new Dictionary<Type, Type>();

            result.Add(typeof(ITicketAgregatorService), typeof(TicketAgregatorService));
            result.Add(typeof(ITicketService), typeof(TicketService));
            result.Add(typeof(IUserService), typeof(UserService));

            return result;
        }
    }
}
