﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using domain.Models.Entities;
using domain.Repositories.Abstractions;
using service.Abstractions;


namespace service.Services
{
    public class TicketAgregatorService : ITicketAgregatorService
    {
        private readonly ITicketAgregator ticketAgregator;
        private readonly IUserRepository userRepository;

        public TicketAgregatorService(IUserRepository userRepository, ITicketAgregator ticketAgregator)
        {
            this.userRepository = userRepository;
            this.ticketAgregator = ticketAgregator;
        }

        public Ticket ChekUserTicket(int ownerId, int ticketId)
        {
            return ticketAgregator.ChekUserTicket(ownerId, ticketId);
        }

        public async Task<EventTicket> Create(EventTicket ticket, int userId)
        {
            var user = await userRepository.Get(userId);
            if(user != null)
            {
                ticket.Owner = user;
                return ticketAgregator.Create(ticket);
            }

            return null;
        }

        public Task<EventTicket> Delete(int ticketId, int ownerId)
        {
            throw new NotImplementedException();
        }

        public Task<EventTicket> Get(int ticketId)
        {
            throw new NotImplementedException();
        }

        public List<EventTicket> Get(int ownerId, int page, int itemsPerPage)
        {
            return ticketAgregator.Get(ownerId, page, itemsPerPage);
        }

        public EventTicketAnalytic GetAnalytic(int ticketId)
        {
            return ticketAgregator.GetAnalytic(ticketId);
        }

        public Task<EventTicket> Update(EventTicket ticket)
        {
            throw new NotImplementedException();
        }
    }
}

