﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using domain.Models.Entities;
using domain.Repositories.Abstractions;
using service.Abstractions;


namespace service.Services
{
    public class TicketService : ITicketService
    {
        private readonly ITicketRepository ticketRepository;
        private readonly IUserRepository userRepository;
        private readonly ITicketAgregator ticketAgregator;

        public TicketService(IUserRepository userRepository, ITicketRepository ticketRepository, ITicketAgregator ticketAgregator)
        {
            this.userRepository = userRepository;
            this.ticketRepository = ticketRepository;
            this.ticketAgregator = ticketAgregator;
        }

        public async Task<Ticket> BookTicket(Ticket ticket, int userId)
        {
            var user = await userRepository.Get(userId);
            if (user != null)
            {
                var ticketAnalytic = ticketAgregator.GetAnalytic(ticket.Event.Id);

                if(ticketAnalytic.AmountOfAvailableTickets > 0)
                {
                    ticket.Owner = user;
                    ticket.Event = ticketAnalytic.EventTicket;
                    return ticketRepository.BookTicket(ticket);
                }

            }

            return null;
        }

        public Ticket GetTicket(int ownerId, int ticketId)
        {
            return ticketRepository.GetTicket(ownerId, ticketId);
        }

        public List<Ticket> GetTickets(TicketFilter filter)
        {
            return ticketRepository.GetTickets(filter);
        }

        public bool IsRealOwner(int ownerId, int ticketId)
        {
            return ticketRepository.IsRealOwner(ownerId, ticketId);
        }
    }
}

