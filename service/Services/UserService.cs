﻿using System;
using System.Threading.Tasks;
using domain.Models.Entities;
using domain.Repositories.Abstractions;
using service.Abstractions;

namespace service.Services
{
    public class UserService: IUserService
    {
        private readonly IUserRepository userRepository;

        public UserService(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public async Task<User> CreateOrUpdate(User user)
        {
            var existinguser = await userRepository.Get(user.Token.UserId, user.Token.Provider);
            if(existinguser != null)
            {
                if (existinguser.Email != user.Email
                   || existinguser.Name != user.Name
                   || existinguser.PictureURL != user.PictureURL
                   || existinguser.Token.TokenString != user.Token.TokenString)
                {
                    return await userRepository.Update(existinguser);
                }
            }
            else
            {
                return await userRepository.Create(user);
            }
            return existinguser;
        }

        public async Task<User> Get(int userId)
        {
            throw new NotImplementedException();
        }
    }
}
