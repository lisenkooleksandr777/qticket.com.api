﻿using System;
using System.Threading.Tasks;
using domain.Models.Entities;

namespace service.Abstractions
{
    public interface IUserService
    {
        Task<User> CreateOrUpdate(User user);

        Task<User> Get(int userId);
    }
}
