﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using domain.Models.Entities;

namespace service.Abstractions
{
    public interface ITicketService
    {
        Task<Ticket> BookTicket(Ticket ticket, int userId);

        List<Ticket> GetTickets(TicketFilter filter);

        Ticket GetTicket(int ownerId, int ticketId);

        bool IsRealOwner(int ownerId, int ticketId);
    }
}

