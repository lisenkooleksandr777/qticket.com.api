﻿using System;
using System.Collections.Generic;
using domain.Repositories.Abstractions;
using repository.Repositories;

namespace repository
{
    public static class Module
    {
        public static Dictionary<Type, Type> GetDependencies()
        {
            var result = new Dictionary<Type, Type>();

            result.Add(typeof(IUserRepository), typeof(UserRepository));
            result.Add(typeof(ITicketAgregator), typeof(TicketAgregator));
            result.Add(typeof(ITicketRepository), typeof(TicketRepository));
            return result;
        }
    }
}
