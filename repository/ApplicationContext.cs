﻿using System;
using domain.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace repository
{
    public class ApplicationContext : DbContext
    {
        public DbSet<SocialNetworkUserToken> SNTTokens { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<EventTicket> EventTickets {get; set;}
        public DbSet<Ticket> Tickets { get; set; }

        public ApplicationContext(DbContextOptions opt) : base(opt) {
            //TODO: to it once, can be here for dev purpose only
            Database.EnsureCreated();
        }        
    }
}
