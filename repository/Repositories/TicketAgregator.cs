﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using domain.Models.Entities;
using domain.Repositories.Abstractions;


namespace repository.Repositories
{
    public class TicketAgregator : ITicketAgregator
    {
        private readonly ApplicationContext db;
        public TicketAgregator(ApplicationContext db)
        {
            this.db = db;
        }

        public EventTicket Create(EventTicket ticket)
        {
            db.EventTickets.Add(ticket);
            db.SaveChanges();

            if(ticket.Id > 0)
            {
                return ticket;
            }
            return null;
        }

        public List<EventTicket> Get(int ownerId, int page, int itemsPerPage)
        {
            var result = db.EventTickets.Where(t => t.Owner.Id == ownerId).Skip(page * itemsPerPage).Take(itemsPerPage).ToList();

            return result;
        }

        public EventTicketAnalytic GetAnalytic(int ticketId)
        {
            var v = (from eventTicket in db.EventTickets
                     where eventTicket.Id == ticketId

                     select new EventTicketAnalytic
                     {
                         EventTicket = eventTicket,
                         
                         // Select the number of shown posts within the forum     
                         AmountOfAvailableTickets = eventTicket.Amount - db.Tickets.Where(t => t.Event.Id == ticketId).Count()
                     }

            ).FirstOrDefault();
            return v;
        }

        public Task<EventTicket> Delete(int ticketId, int ownerId)
        {
            throw new NotImplementedException();
        }

        public Task<EventTicket> Get(int ticketId)
        {
            throw new NotImplementedException();
        }

        public Task<EventTicket> Update(EventTicket ticket)
        {
            throw new NotImplementedException();
        }

        public Ticket ChekUserTicket(int ownerId, int ticketId)
        {
            var scannedTicket = (from ticket in db.Tickets

                join et in db.EventTickets on ticket.Event.Id equals et.Id
                join user in db.Users on ticket.Owner.Id equals user.Id
                where et.Owner.Id == ownerId && ticket.Id == ticketId
                select new Ticket
                {
                    Event = et,
                    Id = ticket.Id,
                    VisitorName = ticket.VisitorName,
                    Owner = user
                }).FirstOrDefault();

            return scannedTicket;
        }
    }
}

