﻿using System;
using System.Linq;
using System.Threading.Tasks;
using domain.Models.Entities;
using domain.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace repository.Repositories
{


    public class UserRepository : IUserRepository
    {

        private readonly ApplicationContext db;
        public UserRepository(ApplicationContext db)
        {
            this.db = db;
        }


        public async Task<User> Create(User user)
        {
            User createdUser = null;
            using (var transaction = db.Database.BeginTransaction())
            {
                db.Users.Add(user);
                db.SaveChanges();

                await transaction.CommitAsync();
                createdUser = user;
            }
            return createdUser;
        }

        public async Task<User> Get(int userId)
        {
            var user = await db.Users.Where(u => u.Id == userId).FirstOrDefaultAsync();
            return user;
        }

        public async Task<User> Get(string socialId, string provider)
        {
            var userToken = await db.SNTTokens.Where(t => t.UserId == socialId && t.Provider == provider).FirstOrDefaultAsync();
                                      
            if (userToken != null)
            {
                var user = await db.Users.Where(t => t.Token.Id == userToken.Id).FirstOrDefaultAsync();

                return user;
            }
            return null;
        }

        public async Task<User> Update(User user)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                db.Users.Update(user);
                db.SaveChanges();

                await transaction.CommitAsync();
            }
            return user;
        }
    }
}
