﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using domain.Models.Entities;
using domain.Repositories.Abstractions;

namespace repository.Repositories
{
    public class TicketRepository : ITicketRepository
    {
        private readonly ApplicationContext db;
        public TicketRepository(ApplicationContext db)
        {
            this.db = db;
        }

        public Ticket BookTicket(Ticket ticket)
        {
            db.Tickets.Add(ticket);
            db.SaveChanges();

            if (ticket.Id > 0)
            {
                return ticket;
            }
            return null;
        }

        public Ticket GetTicket(int ownerId, int ticketId)
        {
            var bookedTicket = (from ticket in db.Tickets

                join et in db.EventTickets on ticket.Event.Id equals et.Id
                    where ticket.Owner.Id == ownerId && ticket.Id == ticketId
                    select new Ticket
                    {
                        Event = et,
                        Id = ticket.Id,
                        VisitorName = ticket.VisitorName,
                        Owner = new User()
                        {
                          Id = ownerId
                        }
                    }).FirstOrDefault();

                return bookedTicket;
        }

        public List<Ticket> GetTickets(TicketFilter filter)
        {
            //get all tickets
            if (!filter.From.HasValue && !filter.To.HasValue)
            {
                var tickets = (from ticket in db.Tickets

                         join et in db.EventTickets on ticket.Event.Id equals et.Id
                         where ticket.Owner.Id == filter.UserId 
                         orderby et.DateStart descending
                         select new Ticket
                         {
                             Event = et,
                             Id = ticket.Id,
                             Owner = new User() {
                                 Id = filter.UserId
                             }
                         }
                        

                ).Skip(filter.Page * filter.ItemsPerPage).Take(filter.ItemsPerPage).ToList();

                return tickets;
            }
            //get tickets future tickets
            else if (filter.To.HasValue && !filter.From.HasValue)
            {
                var tickets = (from ticket in db.Tickets

                join et in db.EventTickets on ticket.Event.Id equals et.Id
                where ticket.Owner.Id == filter.UserId && et.DateEnd < filter.To
                orderby et.DateStart descending
                 select new Ticket
                 {
                    Event = et,
                    Id = ticket.Id,
                    Owner = new User()
                    {
                        Id = filter.UserId
                    }
                 }).Skip(filter.Page * filter.ItemsPerPage).Take(filter.ItemsPerPage).ToList();

                return tickets;
            }
            //get tickets from archive
            else if (filter.From.HasValue  && !filter.To.HasValue)
            {
                var tickets = (from ticket in db.Tickets

                               join et in db.EventTickets on ticket.Event.Id equals et.Id
                               where ticket.Owner.Id == filter.UserId && et.DateStart > filter.From
                               orderby et.DateStart descending
                               select new Ticket
                               {
                                   Event = et,
                                   Id = ticket.Id,
                                   Owner = new User()
                                   {
                                       Id = filter.UserId
                                   }
                               }).Skip(filter.Page * filter.ItemsPerPage).Take(filter.ItemsPerPage).ToList();

                return tickets;
            }
            //get ticket of multiple day ticket
            else if (filter.From.HasValue && filter.To.HasValue)
            {
                var tickets = (from ticket in db.Tickets

                               join et in db.EventTickets on ticket.Event.Id equals et.Id
                               where ticket.Owner.Id == filter.UserId && (
                                (et.DateStart > filter.From && et.DateStart < filter.To)
                                || (et.DateEnd > filter.From && et.DateEnd < filter.To)
                                || (et.DateStart < filter.From && et.DateEnd > filter.To))

                                orderby et.DateStart descending

                               select new Ticket
                               {
                                   Event = et,
                                   Id = ticket.Id,
                                   Owner = new User()
                                   {
                                       Id = filter.UserId
                                   }
                               }).Skip(filter.Page * filter.ItemsPerPage).Take(filter.ItemsPerPage).ToList();

                return tickets;
            }

            return null;
        }

        public bool IsRealOwner(int ownerId, int ticketId)
        {
            var t = (from ticket in db.Tickets
                     where ticket.Id == ticketId  && ticket.Owner.Id == ownerId
                     select ticket).FirstOrDefault();

            return t != null;
        }
    }
}

