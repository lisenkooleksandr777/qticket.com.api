﻿using System;
using API.Adapter.Abstractions;
using domain.Models.Entities;

namespace API.Adapter
{
    public class FilterAdapter : IFilterAdapter
    {
        public TicketFilter ConvertToFilter(int page, int userId, string dateFrom, string dateTo)
        {
            var result = new TicketFilter();
            result.Page = page;
            result.UserId = userId;

            if(!string.IsNullOrEmpty(dateFrom))
            {
                result.From = DateTime.Parse(dateFrom, null);
            }

            if (!string.IsNullOrEmpty(dateTo))
            {
                result.To = DateTime.Parse(dateTo, null);
            }

            return result;
        }
    }
}
