﻿using System;
using System.Collections.Generic;
using API.Adapter.Abstractions;
using API.DTOs;
using domain.Models.Entities;

namespace API.Adapter
{
    public class TicketAdapter : ITicketAdapter
    {

        public List<TicketPreviewDTO> ConvertToTicketPreviewList(List<Ticket> eventTikets)
        {
            var result = new List<TicketPreviewDTO>();

            foreach(var ticket in eventTikets)
            {
                result.Add(
                    new TicketPreviewDTO
                    {
                        Id = ticket.Id,
                        Name = ticket.Event.Name,
                        DateStart = ticket.Event.DateStart.ToString("yyyy-MM-ddTHH:mm:ssZ"),
                        DateEnd = ticket.Event.DateEnd.ToString("yyyy-MM-ddTHH:mm:ssZ"),
                    }
                );
            }

            return result;
        }


        public Ticket ConvertToTicket(TicketDTO ticketDTO)
        {
            var result = new Ticket();

            result.VisitorName = ticketDTO.VisitorName;
            result.Event = new EventTicket
            {
                Id = ticketDTO.Event.Id,
            };

            return result;
        }

        public TicketDTO ConvertToTicketDTO(Ticket ticket)
        {
            var result = new TicketDTO();
            result.VisitorName = ticket.VisitorName;
            result.Id = ticket.Id;
            if (ticket.Owner != null)
            {
                result.Owner = new JWTClaimsDTO()
                {
                    Id = ticket.Owner.Id,
                    Name = ticket.Owner.Name,
                    PictureURL = ticket.Owner.PictureURL,
                };
            }

            if (ticket.Event != null)
            {
                var eventTicketDTO = new EventTicketDTO();

                eventTicketDTO.Id = ticket.Event.Id;
                eventTicketDTO.Name = ticket.Event.Name;
                eventTicketDTO.Amount = ticket.Event.Amount;
                eventTicketDTO.Description = ticket.Event.Description;
                eventTicketDTO.Price = ticket.Event.Price;

                eventTicketDTO.DateStart = ticket.Event.DateStart.ToString("yyyy-MM-ddTHH:mm:ssZ");
                eventTicketDTO.DateEnd = ticket.Event.DateEnd.ToString("yyyy-MM-ddTHH:mm:ssZ");


                if (ticket.Event.Owner != null)
                {

                }
                result.Event = eventTicketDTO;
            }

            return result;
        }
    }
}
