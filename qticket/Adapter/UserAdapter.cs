﻿using System;
using API.Adapter.Abstractions;
using API.DTOs;
using domain.Models.Entities;

namespace API.Adapter
{
    public class UserAdapter : IUserAdapter
    {
        public AuthResponceDTO ConvertToAuthResponce(User user, string tokenString)
        {
            return new AuthResponceDTO
            {
                UserData = new JWTClaimsDTO
                {
                    Id = user.Id,
                    Name = user.Name,
                    PictureURL = user.PictureURL
                },
                Token = tokenString,
            };
        }

        public User ConvertToUser(AuthRequestDTO requestDTO)
        {
            return new User
            {
                Email = requestDTO.Email,
                Name = requestDTO.Name,
                PictureURL = requestDTO.PictureUrl,
                Token = new SocialNetworkUserToken
                {
                    TokenString = requestDTO.GoogleToken.TokenString,
                    Provider = requestDTO.GoogleToken.Provider,
                    UserId = requestDTO.GoogleToken.UserId,
                    TokenId = requestDTO.GoogleToken.TokenId
                }
            };
        }
    }
}
