﻿using System;
using System.Collections.Generic;
using API.Adapter.Abstractions;
using API.DTOs;
using domain.Models.Entities;

namespace API.Adapter
{
    public class EventTicketAdapter : IEventTicketAdapter
    {
        public EventTicket ConvertEventTicket(EventTicketDTO requestDTO)
        {
            var result = new EventTicket();

            result.Amount = requestDTO.Amount;
            result.Name = requestDTO.Name;
            result.Description = requestDTO.Description;
            result.Price = requestDTO.Price;

            result.DateStart = DateTime.Parse(requestDTO.DateStart, null);
            result.DateEnd = DateTime.Parse(requestDTO.DateEnd, null);

            return result;
        }

        public EventTicketDTO ConvertToCreateTicketDTO(EventTicket eventTicket)
        {
            var result = new EventTicketDTO();

            result.Id = eventTicket.Id;
            result.Name = eventTicket.Name;
            result.Amount = eventTicket.Amount;
            result.Description = eventTicket.Description;
            result.Price = eventTicket.Price;

            result.DateStart = eventTicket.DateStart.ToString("yyyy-MM-ddTHH:mm:ssZ");
            result.DateEnd = eventTicket.DateEnd.ToString("yyyy-MM-ddTHH:mm:ssZ");


            if (eventTicket.Owner != null)
            {

            }

            return result;
        }

        public List<EventTicketPreviewDTO> ConvertToEventTicketPreviewList(List<EventTicket> eventTikets)
        {
            var result = new List<EventTicketPreviewDTO>();

            foreach(var ticket in eventTikets)
            {
                result.Add(
                    new EventTicketPreviewDTO
                    {
                        Id = ticket.Id,
                        Name = ticket.Name,
                        DateStart = ticket.DateStart.ToString("yyyy-MM-ddTHH:mm:ssZ"),
                        DateEnd = ticket.DateEnd.ToString("yyyy-MM-ddTHH:mm:ssZ"),
                    }
                );
            }

            return result;
        }

        public QRScannerResultDTO ConvertQRScanResult(EventTicketAnalytic analytic)
        {
            var result = new QRScannerResultDTO();

            result.EventTicketAnalytic = new EventTicketAnalyticDTO();

            result.EventTicketAnalytic.AmountOfAvailableTickets = analytic.AmountOfAvailableTickets;

            result.EventTicketAnalytic.EventTicket = ConvertToCreateTicketDTO(analytic.EventTicket);

            return result;
        }

        public QRScannerResultDTO ConvertQRScanResult(Ticket ticket)
        {
            var qrResult = new QRScannerResultDTO();

            var result = new TicketDTO();
            result.VisitorName = ticket.VisitorName;
            result.Id = ticket.Id;
            if (ticket.Owner != null)
            {
                result.Owner = new JWTClaimsDTO()
                {
                    Id = ticket.Owner.Id,
                    Name = ticket.Owner.Name,
                    PictureURL = ticket.Owner.PictureURL,
                };
            }

            if (ticket.Event != null)
            {
                var eventTicketDTO = new EventTicketDTO();

                eventTicketDTO.Id = ticket.Event.Id;
                eventTicketDTO.Name = ticket.Event.Name;
                eventTicketDTO.Amount = ticket.Event.Amount;
                eventTicketDTO.Description = ticket.Event.Description;
                eventTicketDTO.Price = ticket.Event.Price;

                eventTicketDTO.DateStart = ticket.Event.DateStart.ToString("yyyy-MM-ddTHH:mm:ssZ");
                eventTicketDTO.DateEnd = ticket.Event.DateEnd.ToString("yyyy-MM-ddTHH:mm:ssZ");


                if (ticket.Event.Owner != null)
                {

                }
                result.Event = eventTicketDTO;
            }

            qrResult.Ticket = result ;

            return qrResult;
        }
    }
}
