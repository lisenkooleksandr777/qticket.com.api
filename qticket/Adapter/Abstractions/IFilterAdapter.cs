﻿using System;
using domain.Models.Entities;

namespace API.Adapter.Abstractions
{
    public interface IFilterAdapter
    {
        TicketFilter ConvertToFilter(int page, int userId, string dateFrom, string dateTo);
    }
}
