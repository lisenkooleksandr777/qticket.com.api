﻿using System;
using System.Collections.Generic;
using API.DTOs;
using domain.Models.Entities;

namespace API.Adapter.Abstractions
{
    public interface IEventTicketAdapter
    {
        EventTicket ConvertEventTicket(EventTicketDTO requestDTO);

        EventTicketDTO ConvertToCreateTicketDTO(EventTicket eventTicket);

        List<EventTicketPreviewDTO> ConvertToEventTicketPreviewList(List<EventTicket> eventTikets);

        QRScannerResultDTO ConvertQRScanResult(EventTicketAnalytic analytic);

        QRScannerResultDTO ConvertQRScanResult(Ticket ticket);
    }
}
