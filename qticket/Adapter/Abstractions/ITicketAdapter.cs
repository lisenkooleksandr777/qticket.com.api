﻿using System;
using System.Collections.Generic;
using API.DTOs;
using domain.Models.Entities;

namespace API.Adapter.Abstractions
{
    public interface ITicketAdapter
    {
        Ticket ConvertToTicket(TicketDTO ticketDTO);

        TicketDTO ConvertToTicketDTO(Ticket ticket);

        List<TicketPreviewDTO> ConvertToTicketPreviewList(List<Ticket> eventTikets);
    }
}
