﻿using System;
using API.DTOs;
using domain.Models.Entities;

namespace API.Adapter.Abstractions
{
    public interface IUserAdapter
    {
        User ConvertToUser(AuthRequestDTO requestDTO);

        AuthResponceDTO ConvertToAuthResponce(User user, string tokenString);
    }
}
