﻿using System;
namespace API.Utils.Crypto.Abstractions
{
    public interface ICrypto
    {
        string Encrypt(string clearText);

        string Decrypt(string cipherText);
    }
}
