﻿using System;
namespace API.Utils.QR.Abstractions
{
    public interface IQR
    {
        Byte[] GenerateQRImage(string key, int id);
    }
}
