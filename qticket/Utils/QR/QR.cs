﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using API.Utils.Crypto.Abstractions;
using API.Utils.QR.Abstractions;
using QRCoder;

namespace API.Utils.QR
{
    /// <summary>
    /// Refer to https://dottutorials.net/generate-qr-code-asp-net-core-source-code/
    /// requiered packages:
    ///     "CoreCompat.System.Drawing": "1.0.0-beta006",       
    ///     "ZXing.Net": "0.15.0"
    /// </summary>
    public class QR: IQR
    {
        private readonly ICrypto crypto;
        public QR(ICrypto crypto)
        {
            this.crypto = crypto;
        }

        public byte[] GenerateQRImage(string key, int id)
        {
            Bitmap qrCodeImage = GenerateQR(key, id);

            using (MemoryStream stream = new MemoryStream())
            {
                qrCodeImage.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        public Bitmap GenerateQR(string key, int id)
        {
            QRCodeGenerator _qrCode = new QRCodeGenerator();
            var encryptedText = crypto.Encrypt(String.Format("{0}:{1}", key, id));
            QRCodeData _qrCodeData = _qrCode.CreateQrCode(encryptedText, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(_qrCodeData);
            return qrCode.GetGraphic(20);
        }
    }
}
