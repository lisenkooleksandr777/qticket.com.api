﻿using System;
namespace API.Utils.Configuration.Models
{
    public class AppSettings
    {
        public string Secret { get; set; }

        public string MysqlConnection { get; set; }
    }
}
