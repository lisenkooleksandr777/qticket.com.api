﻿using System;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace API.DTOs
{
    [JsonObject]
    public class AuthResponceDTO
    {
        [JsonPropertyName("token")]
        public string Token { get; set; }

        [JsonPropertyName("userData")]
        public JWTClaimsDTO UserData { get; set; }
    }
}
