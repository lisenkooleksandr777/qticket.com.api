﻿using System;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace API.DTOs
{
    [JsonObject]
    public class GoogleTokenDTO
    {
        [JsonPropertyName("userId")]
        public string UserId { get; set; }

        [JsonPropertyName("provider")]
        public string Provider { get; set; }

        [JsonPropertyName("tokenString")]
        public string TokenString { get; set; }

        [JsonPropertyName("tokenId")]
        public string TokenId { get; set; }
    }
}
