﻿using System;
using Newtonsoft.Json;

namespace API.DTOs
{
    [JsonObject]
    public class TicketPreviewDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("dateStart")]
        public string DateStart { get; set; }

        [JsonProperty("dateEnd")]
        public string DateEnd { get; set; }
    }
}
