﻿using System;
using Newtonsoft.Json;

namespace API.DTOs
{
    [JsonObject]
    public class QRScannerResultDTO
    {
        [JsonProperty("eventTicketAnalytic")]
        public EventTicketAnalyticDTO EventTicketAnalytic { get; set; }

        [JsonProperty("ticket")]
        public TicketDTO Ticket { get;set;}
    }
}
