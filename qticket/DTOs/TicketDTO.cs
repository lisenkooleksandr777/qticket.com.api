﻿using System;
using Newtonsoft.Json;

namespace API.DTOs
{
    [JsonObject]
    public class TicketDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("event")]
        public EventTicketDTO Event { get; set; }

        [JsonProperty("visitorName")]
        public string VisitorName { get; set; }

        [JsonProperty("owner")]
        public JWTClaimsDTO Owner {get;set;}
    
    }
}
