﻿using System;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace API.DTOs
{
    [JsonObject]
    public class JWTClaimsDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("pictureUrl")]
        public string PictureURL { get; set; }
    }
}
