﻿using System;
using Newtonsoft.Json;

namespace API.DTOs
{
    [JsonObject]
    public class EventTicketAnalyticDTO
    {
        [JsonProperty("eventTicket")]
        public EventTicketDTO EventTicket { get; set; }

        [JsonProperty("amountOfAvailableTickets")]
        public int AmountOfAvailableTickets { get; set; }
    }
}
