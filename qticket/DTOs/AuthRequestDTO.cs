﻿using System;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace API.DTOs
{
    [JsonObject]
    public class AuthRequestDTO
    {

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("gender")]
        public string gender { get; set; }

        [JsonPropertyName("birthday")]
        public string Birthday { get; set; }

        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("pictureUrl")]
        public string PictureUrl { get; set; }

        [JsonPropertyName("googleToken")]
        public GoogleTokenDTO GoogleToken { get; set; }
    }
}
