﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using API.Adapter.Abstractions;
using API.DTOs;
using API.Utils.Configuration.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using service.Abstractions;

namespace qticket.Controllers
{
    [Authorize]
    [ApiController]
    [Produces("application/json")]
    [Consumes("application/json")]
    [Route("api/v1/[controller]")]
    public class AuthController : ControllerBase
    {

        private readonly IUserService userService;
        private readonly IUserAdapter userAdapter;
        private readonly AppSettings appSettings;

        public AuthController(IUserService userService, IUserAdapter userAdapter, IOptions<AppSettings> appSettings)
        {
            this.userAdapter = userAdapter;
            this.userService = userService;
            this.appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> AuthBySocialNetwork([FromBody] AuthRequestDTO authRequestDTO)
        {
            try
            {
                //TODO: add validation
                var user = userAdapter.ConvertToUser(authRequestDTO);
                var authUser = await userService.CreateOrUpdate(user);
                if (authUser!= null)
                {
                    // authentication successful so generate jwt token
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes(appSettings.Secret);
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                            new Claim("UserName", authUser.Name),
                            new Claim("UserId", authUser.Id.ToString()),
                            new Claim("PictureUrl", authUser.PictureURL)
                        }),
                        Expires = DateTime.UtcNow.AddDays(7),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                        
                    };
                    var token = tokenHandler.CreateToken(tokenDescriptor);

                    return Ok(userAdapter.ConvertToAuthResponce(authUser, tokenHandler.WriteToken(token)));
                }
                else
                {
                    return StatusCode(500);
                }
               
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
            finally
            {
                //TODO: write some metrics, logs, etc
            }
        }

        [HttpGet]
        public AuthResponceDTO AuthByTocken([FromHeader] string token)
        {
            return null;
        }
    }
}
