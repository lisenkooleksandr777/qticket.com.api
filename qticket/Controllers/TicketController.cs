﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using API.Adapter.Abstractions;
using API.DTOs;
using API.Utils.Configuration.Models;
using API.Utils.QR.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using service.Abstractions;

namespace qticket.Controllers
{
    [Authorize]
    [ApiController]
    [Produces("application/json")]
    [Consumes("application/json")]
    [Route("api/v1/[controller]")]
    public class TicketController : ControllerBase
    {

        private readonly ITicketAdapter ticketAdapter;
        private readonly ITicketService ticketService;
        private readonly IFilterAdapter filterAdapter;
        private readonly IQR qR;
        private readonly AppSettings appSettings;

        public TicketController(IOptions<AppSettings> appSettings,
            ITicketAdapter ticketAdapter,
            ITicketService ticketService,
            IFilterAdapter filterAdapter,
            IQR qR)
        {
            this.appSettings = appSettings.Value;
            this.ticketAdapter = ticketAdapter;
            this.ticketService = ticketService;
            this.filterAdapter = filterAdapter;
            this.qR = qR;
        }

        
        [HttpPost("book")]
        public async Task<IActionResult> BookTicket([FromBody] TicketDTO ticketDTO)
        {
            try
            {
                var ticket = ticketAdapter.ConvertToTicket(ticketDTO);
                var userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId").Value);
                var  bookedTicket = await ticketService.BookTicket(ticket, userId);
                if (bookedTicket != null)
                {
                    return Ok(ticketAdapter.ConvertToTicketDTO(bookedTicket));
                }

                return StatusCode(500);
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
            finally
            {
                //TODO: write some metrics, logs, etc
            }
        }

        [HttpGet("booked")]
        public IActionResult GetBookedTickets([FromQuery] int page, [FromQuery] string dateFrom, [FromQuery] string dateTo)
        {
            try
            {
                var userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId").Value);
                var filter = filterAdapter.ConvertToFilter(page, userId, dateFrom, dateTo);
                var tickets = ticketService.GetTickets(filter);
                if (tickets != null)
                {
                    return Ok(ticketAdapter.ConvertToTicketPreviewList(tickets));
                }

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
            finally
            {
                //TODO: write some metrics, logs, etc
            }
        }


        [HttpGet("{id}")]
        public IActionResult GetBookedTicket(int id)
        {
            try
            {
                var userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId").Value);
                var ticket = ticketService.GetTicket(userId, id);
                if (ticket != null)
                {
                    return Ok(ticketAdapter.ConvertToTicketDTO(ticket));
                }

                return NotFound();
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
            finally
            {
                //TODO: write some metrics, logs, etc
            }
        }


        [HttpGet("qr/{id}")]
        public IActionResult GetBookedTicketQR(int id)
        {
            try
            {
                var userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId").Value);
                if (ticketService.IsRealOwner(userId, id))
                {
                    return File(qR.GenerateQRImage("ticket", id), "image/jpeg");
                }

                return NotFound();
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
            finally
            {
                //TODO: write some metrics, logs, etc
            }
        }
    }
}
