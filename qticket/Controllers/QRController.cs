﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using API.Adapter.Abstractions;
using API.DTOs;
using API.Utils.Configuration.Models;
using API.Utils.Crypto.Abstractions;
using API.Utils.QR.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using service.Abstractions;

namespace qticket.Controllers
{
    [Authorize]
    [ApiController]
    [Produces("application/json")]
    [Consumes("application/json")]
    [Route("api/v1/[controller]")]
    public class QRController : ControllerBase
    {

        private readonly ITicketAgregatorService ticketAgregatorService;
        private readonly IEventTicketAdapter ticketAdapter;
        private readonly ICrypto crypto;

        public QRController(ICrypto crypto, ITicketAgregatorService ticketAgregatorService, IEventTicketAdapter ticketAdapter)
        {
            this.crypto = crypto;
            this.ticketAgregatorService = ticketAgregatorService;
            this.ticketAdapter = ticketAdapter;
        }

   
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTicketByQR(string id)
        {
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    var data = crypto.Decrypt(id);
                    var tokens = data.Split(":");
                    if (tokens.Length == 2)
                    {
                        if (tokens[0] == "eventTicket")
                        {
                            int eventTiketId = 0;
                            int.TryParse(tokens[1], out eventTiketId);

                            if (eventTiketId > 0)
                            {
                                var analytic = ticketAgregatorService.GetAnalytic(eventTiketId);
                                return Ok(ticketAdapter.ConvertQRScanResult(analytic));
                            }
                        }
                        else if(tokens[0] == "ticket")
                        {
                            int ticketId = 0;
                            int.TryParse(tokens[1], out ticketId);
                            var userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId").Value);
                            if (ticketId > 0)
                            {
                                var scannedTicket = ticketAgregatorService.ChekUserTicket(userId, ticketId);
                                if (scannedTicket != null)
                                {
                                    return Ok(ticketAdapter.ConvertQRScanResult(scannedTicket));
                                }
                            }
                        }
                    }
                }
                return BadRequest();
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
            finally
            {
                //TODO: write some metrics, logs, etc
            }
        }
    }
}
