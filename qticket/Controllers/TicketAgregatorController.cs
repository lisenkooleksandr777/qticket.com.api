﻿using System;
using System.Linq;
using System.Threading.Tasks;
using API.Adapter.Abstractions;
using API.DTOs;
using API.Utils.Configuration.Models;
using API.Utils.QR.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using service.Abstractions;

namespace qticket.Controllers
{
    [Authorize]
    [ApiController]
    [Produces("application/json")]
    [Consumes("application/json")]
    [Route("api/v1/[controller]")]
    public class TicketAgregatorController : ControllerBase
    {

        private readonly ITicketAgregatorService ticketAgregatorService;
        private readonly IEventTicketAdapter ticketAdapter;
        private readonly IQR qR;
        public TicketAgregatorController(ITicketAgregatorService ticketAgregatorService, IEventTicketAdapter ticketAdapter, IQR qR)
        {
            this.ticketAdapter = ticketAdapter;
            this.ticketAgregatorService = ticketAgregatorService;
            this.qR = qR;
        }

        [HttpPost]
        public async Task<IActionResult> CreateEventTicket([FromBody] EventTicketDTO createEventTicketRequestDTO)
        {
            try
            {
                //TODO: add validation
                var eventTicket = ticketAdapter.ConvertEventTicket(createEventTicketRequestDTO);
                var userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId").Value);   
                var ticket = await ticketAgregatorService.Create(eventTicket, userId);
                if (ticket != null)
                {
                    return Ok(ticketAdapter.ConvertToCreateTicketDTO(ticket));
                }

                return StatusCode(500);
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
            finally
            {
                //TODO: write some metrics, logs, etc
            }
        }

        [HttpGet]
        public IActionResult GetOwnEventTickets([FromQuery] int page = 0, int itemsPerPage = 10)
        {
            try
            {
                var userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId").Value);
                var tickets = ticketAgregatorService.Get(userId, page, itemsPerPage);
                if (tickets != null && tickets.Count > 0)
                {
                    return Ok(ticketAdapter.ConvertToEventTicketPreviewList(tickets));
                }

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
            finally
            {
                //TODO: write some metrics, logs, etc
            }
        }

        [AllowAnonymous]
        [HttpGet("qr/{id}")]
        public IActionResult GenerateEventTicketQRCode(int id)
        {
            try
            {
               if(id > 0)
               {
                    return File(qR.GenerateQRImage("eventTicket", id), "image/jpeg");
               }

                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
            finally
            {
                //TODO: write some metrics, logs, etc
            }
        }
    }
}
